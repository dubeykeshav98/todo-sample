(function(){

	'use strict';

	angular.module("TodoApp",['ngRoute'])
	.config(['$routeProvider',function($routeProvider){
		$routeProvider
		.when('/create',{
			templateUrl : 'modules/createToDo/views/ToDoHome.html',
			controller:'ToDoController',
			controllerAs:'controller'
		})
		.when('/show',{
			templateUrl : 'modules/createToDo/views/ToDoDetails.html',
			controller:'ToDoController',
			controllerAs:'controller'
		})
		.when('/edit',{
			templateUrl: 'modules/createToDo/views/ToDoEdit.html',
			controller:'ToDoController',
			controllerAs:'controller'
		})
		.otherwise({
			templateUrl : 'modules/createToDo/views/ToDoHome.html'
		});
	}])
	.controller("ToDoController",ToDoController)
	.service("ToDoService",ToDoService);

	ToDoController.$inject = ["ToDoService"];
	function ToDoController(ToDoService){
		var controller = this;
		controller.items = ToDoService.getItems();
		controller.subtasks = [];
		controller.inputFields = [];

		controller.addInputField  =function(){
			controller.inputFields.push({});
		}

		controller.removeInputField = function(index){
			controller.inputFields.splice(controller.inputFields.length-1,1);
		}

		controller.addItem = function(task){
			ToDoService.addItem(task,controller.subtasks);
			controller.subtasks = [];
		}

		controller.editItem = function(task,index){
			ToDoService.editItem(task,controller.subtasks,index);
		}

		controller.deleteItem = function(index){
			ToDoService.removeItem(index);
		}

		controller.addSub = function(subtask){
			controller.subtasks.push(subtask);
		}

		
	}

	function ToDoService(){
		var service = this;
		var items = [];

		service.addItem = function(task,subtask){
			var item = {tasks: task, subtasks:subtask};
			items.push(item);
		};

		service.editItem = function(task,subtask,index){
			var item = {tasks: task, subtasks:subtask};	
			items[index] = item;
		}

		service.removeItem = function(index){
			items.splice(index,1);
		};

		service.getItems = function(){
			return items;
		};
	}

})();